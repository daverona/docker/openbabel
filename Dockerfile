FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# Install openbabel dependencies
RUN apt-get update \
  && apt-get install --yes --quiet --no-install-recommends \
    libboost-filesystem1.65.1 \
    libboost-iostreams1.65.1 \
    libboost-program-options1.65.1 \
    libboost-regex1.65.1 \
    libboost-system1.65.1 \
    libboost-test1.65.1 \
    libpython3.6 \
    libxml2 \
    python3 \
    python3-cairo \
    swig \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

ARG OPENBABEL_VERSION=3.1.1
ARG OPENBABEL_HOME=/usr/local/openbabel/$OPENBABEL_VERSION
SHELL ["/bin/bash", "-c"]

# Install openbabel
RUN build_deps="\
    build-essential \
    cmake \
    libboost-filesystem1.65-dev \
    libboost-iostreams1.65-dev \
    libboost-program-options1.65-dev \
    libboost-regex1.65-dev \
    libboost-system1.65-dev \
    libboost-test1.65-dev \
    libboost1.65-dev \
    libcairo2-dev \
    libeigen3-dev \
    libxml2-dev \
    python3-dev \
    rapidjson-dev \
    wget \
    zlib1g-dev" \
  && apt-get update \
  && apt-get install --yes --quiet $build_deps \
  && wget --quiet --no-hsts --output-document=- https://github.com/openbabel/openbabel/archive/openbabel-${OPENBABEL_VERSION//./-}.tar.gz | tar -zxvf - -C /tmp \
  && mkdir -p /tmp/openbabel-openbabel-${OPENBABEL_VERSION//./-}/build \
  && cd /tmp/openbabel-openbabel-${OPENBABEL_VERSION//./-}/build \
  && cmake .. \
    -Wno-dev \
    -DRUN_SWIG=ON \
    -DPYTHON_BINDINGS=ON \
    -DPYTHON_EXECUTABLE=/usr/bin/python3.6 \
    -DCMAKE_INSTALL_PREFIX=$OPENBABEL_HOME \
    -DCMAKE_BUILD_TYPE=Release \
  && make -j $(nproc) && make install \
  # This is a heck to pass Test #218, which fails
  # because \N in a string is interpreted for unicode
  # but in our case \N has nothing to do with unicode.
  # So we make the string as a raw string.
  && sed -i -e "45s|'C|r'C|" -e "50s|'C|r'C|" /tmp/openbabel-openbabel-${OPENBABEL_VERSION//./-}/test/testdistgeom.py \
  && make test \
  && ln -s $OPENBABEL_HOME/lib/python3.6/site-packages/openbabel /usr/local/lib/python3.6/dist-packages/openbabel \
  && cd / && rm -rf /tmp/* \
  && apt-get purge --yes --auto-remove $build_deps \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# Set environment variables
ENV LD_LIBRARY_PATH=$OPENBABEL_HOME/lib:$LD_LIBRARY_PATH
ENV PATH=$OPENBABEL_HOME/bin:$PATH

WORKDIR /var/local

CMD ["/usr/bin/python3"]
