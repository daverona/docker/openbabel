# daverona/openbabel <!-- docker openbabel docker -->

[`ubuntu`](https://gitlab.com/daverona/docker/openbabel)
[![pipeline status](https://gitlab.com/daverona/docker/openbabel/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/openbabel/commits/master)

[`alpine`](https://gitlab.com/daverona/docker/openbabel/-/tree/alpine)
[![pipeline status](https://gitlab.com/daverona/docker/openbabel/badges/alpine/pipeline.svg)](https://gitlab.com/daverona/docker/openbabel/commits/alpine)

This is a repository for Docker images of [Open Babel](https://github.com/openbabel/openbabel) library.

* GitLab source repository: [https://gitlab.com/daverona/docker/openbabel](https://gitlab.com/daverona/docker/openbabel)
* Docker Hub repository: [https://hub.docker.com/r/daverona/openbabel](https://hub.docker.com/r/daverona/openbabel)

Available versions are:

| Version | Ubuntu | Alpine |
|---|---|---|
| 3.1.1 | [3.1.1](https://gitlab.com/daverona/docker/openbabel/-/blob/3.1.1/Dockerfile), [latest](https://gitlab.com/daverona/docker/openbabel/-/blob/3.1.1/Dockerfile) | [3.1.1-alpine3.10](https://gitlab.com/daverona/docker/openbabel/-/blob/3.1.1-alpine3.10/Dockerfile)
| 3.1.0 | [3.1.0](https://gitlab.com/daverona/docker/openbabel/-/blob/3.1.0/Dockerfile)| [3.1.0-alpine3.10](https://gitlab.com/daverona/docker/openbabel/-/blob/3.1.0-alpine3.10/Dockerfile) |
| 3.0.0 | [3.0.0](https://gitlab.com/daverona/docker/openbabel/-/blob/3.0.0/Dockerfile)| [3.0.0-alpine3.10](https://gitlab.com/daverona/docker/openbabel/-/blob/3.0.0-alpine3.10/Dockerfile) |

## Installation

Pull the image from Docker Hub repository:

```bash
docker image pull daverona/openbabel
```

## Quick Start

Run the container:

```bash
docker container run --rm \
  daverona/openbabel \
  python3 -c "import openbabel;print(openbabel.__version__)"
```

It will show the version of openbabel built in the container.

If you want a Python 3 shell with openbabel available, run the container:

```bash
docker container run --rm \
  --interactive --tty \
  daverona/openbabel
```

## Advanced Usages

### Copy to Another Docker Image

To copy Open Babel from this image to yours *not as* base image, add the following:

```dockerfile
# Replace 3.1.1 with the version you want on the next two lines
ARG OPENBABEL_HOME=/usr/local/openbabel/3.1.1
COPY --from=daverona/openbabel:3.1.1 $OPENBABEL_HOME $OPENBABEL_HOME
RUN apt-get update \
  && apt-get install --yes --quiet --no-install-recommends \
    libboost-filesystem1.65.1 \
    libboost-iostreams1.65.1 \
    libboost-program-options1.65.1 \
    libboost-regex1.65.1 \
    libboost-system1.65.1 \
    libboost-test1.65.1 \
    libpython3.6 \
    libxml2 \
    python3 \
    python3-cairo \
    swig \
  && apt-get clean && rm -rf /var/lib/apt/lists/* \
  && ln -s $OPENBABEL_HOME/lib/python3.6/site-packages/openbabel /usr/local/lib/python3.6/dist-packages/openbabel
ENV LD_LIBRARY_PATH=$OPENBABEL_HOME/lib:$LD_LIBRARY_PATH
ENV PATH=$OPENBABEL_HOME/bin:$PATH
```

## References

* Open Babel source repository: [https://github.com/openbabel/openbabel](https://github.com/openbabel/openbabel)
* Open Babel documentation: [https://open-babel.readthedocs.io/](https://open-babel.readthedocs.io/)
